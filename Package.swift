// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription


let package = Package(
    name: "ExampleRust",
    platforms: [
        .iOS(.v15)
    ],
    products: [
        .library(
            name: "ExampleRust",
            targets: ["ExampleRust"]),
    ],
    targets: [
        .binaryTarget(
           name: "ExampleRustBindings",
           path: "ExampleRustBindings.xcframework"),
        /*
         * A placeholder wrapper for our binaryTarget so that Xcode will ensure this is
         * downloaded/built before trying to use it in the build process
         * A bit hacky but necessary for now https://github.com/mozilla/application-services/issues/4422
         */
        .target(
            name: "ExampleRustBindingsWrapper",
            dependencies: [
                .target(name: "ExampleRustBindings")
            ],
            path: "ExampleRustBindingsWrapper"
        ),
        .target(
            name: "ExampleRust",
            dependencies: ["ExampleRustBindingsWrapper"]),
        // .testTarget(
        //     name: "ExampleRustBindingsTests",
        //     dependencies: ["ExampleRustBindingsFFI"]),
    ]
)
